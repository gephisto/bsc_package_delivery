package com.company;

import com.company.deliverycompany.Delivery;
import com.company.deliverycompany.Packet;
import com.company.deliverycompany.PacketFee;
import com.company.filetools.FileLoader;

import java.util.List;
import java.util.Scanner;

public class Main {

    /**
     * There are created test files in ./testFiles/
     * example arguments: "./testFiles/deliveryInitValid.txt" "./testFiles/feeTaxesValid.txt"
     * @param args input files
     */
    public static void main(String[] args) {
        InputValidator.validateInputArguments(args);

        FileLoader fileLoader = new FileLoader(args[0], args.length > 1 ? args[1] : null);
        List<Packet> inputPackets = fileLoader.parseInputPackets();

        if(inputPackets == null){
            System.out.println("File with initial load can't be null and must be parsable.");
            return;
        }

        List<PacketFee> fees = fileLoader.parseFees();
        if(fees == null){
            System.out.println("Fee setup is not use or file is not parsable! Delivery will be work without fees.");
        }

        Delivery delivery = new Delivery(inputPackets, fees);
        Scanner s = new Scanner(System.in);
        while(true){
            String input = s.nextLine();
            if(input.equals("quit")){
                delivery.stopDelivering();
                return;
            }
            Packet pack = Packet.createPacket(input);
            if(pack != null){
                delivery.addIntoDelivery(pack);
            }else{
                System.err.println("Invalid input, packet not created.");
            }
        }
    }
}
