package com.company;

import java.io.File;

/**
 * Validator for application start
 */
public class InputValidator {
    /**
     * Validate application params input
     * @param args arguments for validation
     */
    public static void validateInputArguments(String[] args){
        if(args.length == 0){
            throw new IllegalArgumentException("On input must be specify file with delivery init state!");
        }
        if(args.length > 2){
            throw new IllegalArgumentException("Input arguments can be only two!");
        }

        for(String arg : args){
            File file = new File(arg);
            if(!file.canRead()){
                throw new IllegalArgumentException("Cant find input file: \"" + args[0] + "\"!");
            }
        }
    }
}
