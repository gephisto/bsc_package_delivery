package com.company.filetools;

import com.company.deliverycompany.Packet;
import com.company.deliverycompany.PacketFee;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class FileLoader {
    private File initState;
    private File feeTable;

    public FileLoader(String initStatePath, String feeTablePath) {
        this.initState = new File(initStatePath);
        if(feeTablePath != null){
            this.feeTable = new File(feeTablePath);
        }
    }

    public List<Packet> parseInputPackets() {
        List<String> inputLines = parseLinesFromFile(initState);
        if(inputLines == null){
            System.err.println("Error while loading init state.");
            return null;
        }

        List<Packet> inputState = new ArrayList<>();
        for(String inputLine : inputLines){
            Packet packet = Packet.createPacket(inputLine);
            if(packet == null){
                System.err.println("Error in creating Packet from line \"" + inputLine + "\" in input file. Cant create data from this file." );
                return null;
            }
            inputState.add(packet);
        }
        return inputState;
    }

    public List<PacketFee> parseFees() {
        List<String> inputLines = parseLinesFromFile(feeTable);
        if(inputLines == null){
            System.err.println("Error while loading fee table.");
            return null;
        }

        List<PacketFee> fees = new ArrayList<>();
        for(String inputLine : inputLines){
            PacketFee fee = PacketFee.createFee(inputLine);
            if(fee == null){
                System.err.println("Error in creating Fee from line \"" + inputLine + "\" in input file. Cant create data from this file." );
                return null;
            }
            fees.add(fee);
        }

        Set<BigDecimal> weights = fees.stream().map(PacketFee::getMinimalWeight).collect(Collectors.toSet());
        if(fees.size() != weights.size()){
            System.err.println("This file cannot be use for creating fee table, because there are two or more same weights." );
            return null;
        }
        return fees;
    }

    private List<String> parseLinesFromFile(File inputFile) {
        if(!inputFile.canRead()){
            return null;
        }
        List<String> loadedLines = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String input;

            while((input = reader.readLine()) != null){
                loadedLines.add(input);
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Problem when reading input file: " + inputFile.getName() + "\n" +e );
            return null;
        }
        return loadedLines;
    }
}
