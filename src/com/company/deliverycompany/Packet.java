package com.company.deliverycompany;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * Class represent packet which is loaded from console input or init input file
 */
public class Packet {
    private static final String PACKET_INPUT_PATTERN  = "^[0-9]+([.][0-9]{1,3})? [0-9]{5}$";

    private final BigDecimal weight;
    private final Integer postCode;

    public Packet(BigDecimal weight, Integer postCode) {
        this.weight = weight;
        this.postCode = postCode;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public Integer getPostCode() {
        return postCode;
    }

    /**
     * Try parse and create instance of @Packet from input string
     * @param input input string
     * @return created Packet if can be parsed from input
     */
    public static Packet createPacket(String input){
        if(!Pattern.matches(PACKET_INPUT_PATTERN, input)){
            return null;
        }
        String[] packetSplit = input.split(" ");
        BigDecimal weight;
        int postCode;
        try{
            weight = new BigDecimal(packetSplit[0]);
            if(BigDecimal.ZERO.compareTo(weight) == 0){
                System.err.println("Weight must be positive number.");
                return null;
            }
            postCode = Integer.parseInt(packetSplit[1]);
        } catch (NumberFormatException e) {
            return null;
        }
        return new Packet(weight, postCode);
    }

    @Override
    public String toString() {
        return "Packet: weight="+this.weight+", postCode="+postCode;
    }
}
