package com.company.deliverycompany;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * Class represent fee by minimal weight of packet
 */
public class PacketFee {
    private static final String FEE_INPUT_PATTERN  = "^[0-9]+([.][0-9]{1,3})? [0-9]+([.][0-9]{2})$";

    private final BigDecimal minimalWeight;
    private final BigDecimal fee;

    public PacketFee(BigDecimal minimalWeight, BigDecimal fee) {
        this.minimalWeight = minimalWeight;
        this.fee = fee;
    }


    /**
     * Try parse and create instance of @PacketFee from input string
     * @param inputLine input for parsing
     * @return return Fee for weight
     */
    public static PacketFee createFee(String inputLine){
        if(!Pattern.matches(FEE_INPUT_PATTERN, inputLine)){
            return null;
        }
        String[] packetSplit = inputLine.split(" ");
        BigDecimal weight;
        BigDecimal fee;
        try{
            weight = new BigDecimal(packetSplit[0]);
            if(BigDecimal.ZERO.compareTo(weight) == 0){
                System.err.println("Weight must be positive number.");
                return null;
            }
            fee = new BigDecimal(packetSplit[1]);
        } catch (NumberFormatException e) {
            return null;
        }
        return new PacketFee(weight, fee);
    }

    public BigDecimal getMinimalWeight() {
        return minimalWeight;
    }

    public BigDecimal getFee() {
        return fee;
    }

    @Override
    public String toString() {
        return "PacketFee: weightFrom="+this.minimalWeight+", fee="+fee;
    }
}
