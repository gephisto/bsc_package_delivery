package com.company.deliverycompany;

import java.math.BigDecimal;

/**
    Data structure for hold weight and fee
 */
public class WeightWithFee {

    private BigDecimal weight;
    private BigDecimal fee;

    public WeightWithFee(BigDecimal weight, BigDecimal fee) {
        this.weight = weight;
        this.fee = fee;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }
}
