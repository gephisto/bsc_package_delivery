package com.company.deliverycompany.tests;

import com.company.deliverycompany.PacketFee;
import org.junit.jupiter.api.Test;

class PacketFeeTest {

    @Test
    void testWeight(){
        assert PacketFee.createFee("2.2") == null;
        assert PacketFee.createFee(" 2.20") == null;
        assert PacketFee.createFee("  2.20") == null;
        assert PacketFee.createFee("1 2.20") != null;
        assert PacketFee.createFee("1.1 2.20") != null;
        assert PacketFee.createFee("1.12 2.20") != null;
        assert PacketFee.createFee("1.123 2.20") != null;
        assert PacketFee.createFee("1.1234 2.20") == null;

        assert PacketFee.createFee(" 1 2.20") == null;
        assert PacketFee.createFee("a.123 2.20") == null;
        assert PacketFee.createFee("a 2.20") == null;
        assert PacketFee.createFee("1.1a34 2.20") == null;

        assert PacketFee.createFee("0.0 2.20") == null;
    }

    @Test
    void testFee(){
        assert PacketFee.createFee("2.2") == null;
        assert PacketFee.createFee("2.2 ") == null;
        assert PacketFee.createFee("2.2  ") == null;

        assert PacketFee.createFee("2.2 1") == null;
        assert PacketFee.createFee("2.2 1.") == null;
        assert PacketFee.createFee("2.2 1.1") == null;
        assert PacketFee.createFee("2.2 1.11") != null;
        assert PacketFee.createFee("2.2 1.111") == null;
        assert PacketFee.createFee("2.2 1.11 ") == null;

        assert PacketFee.createFee("2.2 a.11") == null;
        assert PacketFee.createFee("2.2 1.a1") == null;
        assert PacketFee.createFee("2.2 1.11a") == null;
    }
}