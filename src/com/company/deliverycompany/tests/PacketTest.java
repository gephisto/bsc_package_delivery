package com.company.deliverycompany.tests;

import com.company.deliverycompany.Packet;
import org.junit.jupiter.api.Test;

public class PacketTest {

    @Test
    void testWeight(){
        assert Packet.createPacket(" 12312") == null;
        assert Packet.createPacket("1 12312") != null;
        assert Packet.createPacket("1.1 12312") != null;
        assert Packet.createPacket("1.12 12312") != null;
        assert Packet.createPacket("1.123 12312") != null;
        assert Packet.createPacket("1.1234 12312") == null;

        assert Packet.createPacket(" 1 12312") == null;
        assert Packet.createPacket("a.123 12312") == null;
        assert Packet.createPacket("a 12312") == null;
        assert Packet.createPacket("1.1a34 12312") == null;

        assert Packet.createPacket("0.0 12312") == null;
    }

    @Test
    void testPostCode(){
        assert Packet.createPacket("1.1 ") == null;
        assert Packet.createPacket("1.1 1") == null;
        assert Packet.createPacket("1.1 123") == null;
        assert Packet.createPacket("1.1 1234") == null;
        assert Packet.createPacket("1.1 12345") != null;
        assert Packet.createPacket("1.1 123456") == null;

        assert Packet.createPacket("1.1 a") == null;
        assert Packet.createPacket("1.1 12a45") == null;

        assert Packet.createPacket("1.1 12345 ") == null;
    }

    @Test
    void testSeparator(){
        assert Packet.createPacket("1.1 12345") != null;
        assert Packet.createPacket("1,1 12345") == null;

    }
}
