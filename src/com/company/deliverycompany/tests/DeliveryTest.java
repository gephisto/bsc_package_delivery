package com.company.deliverycompany.tests;

import com.company.deliverycompany.Delivery;
import com.company.deliverycompany.Packet;
import com.company.deliverycompany.PacketFee;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class DeliveryTest {

    private static final List<Packet> EMPTY_INIT_STATE = Collections.emptyList();
    private static final List<Packet> NOT_EMPTY_INIT_STATE = Arrays.asList(Packet.createPacket("1.5 12345"));

    private static final List<PacketFee> PACKET_FEE_TABLE = Arrays.asList(PacketFee.createFee("1 1.00"), PacketFee.createFee("2 2.00"));

    private static final Packet testPacket = Packet.createPacket("1.5 12345");
    private static final Packet testPacket2 = Packet.createPacket("2.5 12345");
    private static final Packet testPacket3 = Packet.createPacket("2.5 22345");

    @Test
    void testCreatingDeliveryEmptyInitState(){
        Delivery delivery = new Delivery(EMPTY_INIT_STATE, null);
        assert  delivery.getPacketsGroupByPost().size() == 0;

        assert testPacket != null;
        delivery.addIntoDelivery(testPacket);
        assert  delivery.getPacketsGroupByPost().size() == 1;
    }

    @Test
    void testCreatingDeliveryNotEmptyInitState(){
        Delivery delivery = new Delivery(NOT_EMPTY_INIT_STATE, null);
        assert  delivery.getPacketsGroupByPost().size() == 1;

        assert testPacket != null;
        delivery.addIntoDelivery(testPacket);
        assert  delivery.getPacketsGroupByPost().size() == 2;
    }

    @Test
    void testCreatingDeliveryWithoutFeeTable(){
        Delivery delivery = new Delivery(NOT_EMPTY_INIT_STATE, null);
        assert delivery.getPacketsGroupByPost().size() == 1;
        assert delivery.getPacketsGroupByPost().values().iterator().next().getFee().equals(BigDecimal.ZERO);
    }

    @Test
    void testCreatingDeliveryWithFeeTable(){
        Delivery delivery = new Delivery(NOT_EMPTY_INIT_STATE, PACKET_FEE_TABLE);
        assert delivery.getPacketsGroupByPost().size() == 1;
        assert !delivery.getPacketsGroupByPost().values().iterator().next().getFee().equals(BigDecimal.ZERO);
    }

    @Test
    void addingIntoDelivery(){
        assert testPacket != null;
        assert testPacket2 != null;
        assert testPacket3 != null;

        Delivery delivery = new Delivery(EMPTY_INIT_STATE, PACKET_FEE_TABLE);
        delivery.addIntoDelivery(testPacket);
        assert delivery.getPacketsGroupByPost().size() == 1;
        assert delivery.getPacketsGroupByPost().values().iterator().next().getWeight().equals(testPacket.getWeight());
        assert delivery.getPacketsGroupByPost().values().iterator().next().getFee().equals(PACKET_FEE_TABLE.get(0).getFee());

        delivery.addIntoDelivery(testPacket2);
        assert delivery.getPacketsGroupByPost().size() == 1; //adding into by same postCode
        assert delivery.getPacketsGroupByPost().values().iterator().next().getWeight().equals(testPacket.getWeight().add(testPacket2.getWeight()));
        assert delivery.getPacketsGroupByPost().values().iterator().next().getFee().equals(PACKET_FEE_TABLE.get(0).getFee().add(PACKET_FEE_TABLE.get(1).getFee()));
        Integer key = delivery.getPacketsGroupByPost().keySet().iterator().next();

        delivery.addIntoDelivery(testPacket3);
        assert delivery.getPacketsGroupByPost().size() == 2; //adding into by same postCode
        assert delivery.getPacketsGroupByPost().get(key).getWeight().equals(testPacket.getWeight().add(testPacket2.getWeight()));
        assert delivery.getPacketsGroupByPost().get(key).getFee().equals(PACKET_FEE_TABLE.get(0).getFee().add(PACKET_FEE_TABLE.get(1).getFee()));
    }
}