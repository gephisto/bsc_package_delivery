package com.company.deliverycompany;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Class which represent delivery company,
 * there are information about sum of weight and fees grouped by post (postCode)
 * When is instance created than start thread which list actual state every interval specify by field parameter LIST_INTERVAL
 */
public class Delivery implements Runnable{
    private static final int LIST_INTERVAL = 60000;

    private volatile boolean stopListingDelivery;
    private final Hashtable<Integer, WeightWithFee> packetsGroupByPost;
    private final List<PacketFee> fees;

    public Delivery(List<PacketFee> fees) {
        this.fees = fees;
        this.packetsGroupByPost = new Hashtable<>();

        Thread listingThread = new Thread(this, "listing delivery");
        this.stopListingDelivery = false;
        listingThread.start();
    }

    public Delivery(List<Packet> initPackets, List<PacketFee> fees){
        this(fees);
        initPackets.forEach(this::addIntoDelivery);
        printDelivery();
    }

    /**
     * Add packet into delivery and add weight and fee ordered by postCode
     * @param packet inserting packet
     */
    public void addIntoDelivery(Packet packet){
        if(packetsGroupByPost.get(packet.getPostCode()) == null){
            packetsGroupByPost.put(packet.getPostCode(), new WeightWithFee(packet.getWeight(), findFee(packet.getWeight())));
        }else{
            BigDecimal finalWeight = packetsGroupByPost.get(packet.getPostCode()).getWeight().add(packet.getWeight());
            BigDecimal finalFee = packetsGroupByPost.get(packet.getPostCode()).getFee().add(findFee(packet.getWeight()));
            packetsGroupByPost.get(packet.getPostCode()).setWeight(finalWeight);
            packetsGroupByPost.get(packet.getPostCode()).setFee(finalFee);
        }
    }

    private BigDecimal findFee(BigDecimal weight) {
        if(fees == null || fees.isEmpty()){
            return BigDecimal.ZERO;
        }

        BigDecimal calculatedFee = fees.stream()
                .filter(fee -> fee.getMinimalWeight().compareTo(weight) < 1)
                .max(Comparator.comparing(PacketFee::getFee))
                .map(PacketFee::getFee)
                .orElse(null);
        if(calculatedFee == null){
            calculatedFee = fees.stream().min(Comparator.comparing(PacketFee::getFee)).map(PacketFee::getFee).orElseThrow(IllegalStateException::new);
        }
        return calculatedFee;
    }

    @Override
    public void run() {
        while(!stopListingDelivery){
            try {
                Thread.sleep(LIST_INTERVAL);
                printDelivery();
            } catch (InterruptedException e) {
                System.err.println("Delivery listing problem: "+e);
            }
        }
    }

    public void printDelivery() {
        if(packetsGroupByPost.entrySet().isEmpty()){
            System.out.println("Delivery is empty");
        }else{
            System.out.println("\nPackets in delivery:");
        }

        packetsGroupByPost.entrySet().stream()
                .sorted((e1, e2) -> e2.getValue().getWeight().compareTo(e1.getValue().getWeight())) //order by weight desc
                .forEach(this::printDeliveryByPostCode);
        System.out.println();
    }

    private void printDeliveryByPostCode(Map.Entry<Integer, WeightWithFee> entry) {
        if(fees == null || fees.isEmpty()){
            System.out.println(String.format("%05d" , entry.getKey()) + " " + entry.getValue().getWeight().setScale(3));
        }else{
            System.out.println(String.format("%05d" , entry.getKey()) + " " + entry.getValue().getWeight().setScale(3) + " " + entry.getValue().getFee());
        }
    }

    public void stopDelivering() {
        this.stopListingDelivery = true;
    }

    public Hashtable<Integer, WeightWithFee> getPacketsGroupByPost() {
        return packetsGroupByPost;
    }
}
