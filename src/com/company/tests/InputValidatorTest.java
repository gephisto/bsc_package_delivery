package com.company.tests;

import com.company.InputValidator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

class InputValidatorTest {
    private static final String TEST_FILE_PATH = "./testFileForInputValidator";

    @BeforeAll
    static void createTestFile(){
        deleteTestFile();

        File testFile = new File(TEST_FILE_PATH);
        if(!testFile.exists()) {
            try {
                assert testFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AfterAll
    static void deleteTestFile(){
        File testFile = new File(TEST_FILE_PATH);
        if(testFile.exists()){
            try {
                assert testFile.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    void testArguments(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> InputValidator.validateInputArguments(new String[]{}));
        Assertions.assertThrows(IllegalArgumentException.class, () -> InputValidator.validateInputArguments(new String[]{""}));
        Assertions.assertThrows(IllegalArgumentException.class, () -> InputValidator.validateInputArguments(new String[]{"a"}));

        Assertions.assertDoesNotThrow(() -> InputValidator.validateInputArguments(new String[]{TEST_FILE_PATH}));
        Assertions.assertDoesNotThrow(() -> InputValidator.validateInputArguments(new String[]{TEST_FILE_PATH, TEST_FILE_PATH}));
        Assertions.assertThrows(IllegalArgumentException.class, () -> InputValidator.validateInputArguments(new String[]{TEST_FILE_PATH, TEST_FILE_PATH, TEST_FILE_PATH}));
    }

}